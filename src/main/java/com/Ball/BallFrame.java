package com.Ball;

import javax.swing.*;
import java.awt.*;



public class BallFrame extends JFrame{

    public JLabel jLabel = new JLabel();

    public JLabel coordinates = new JLabel();

    public BallPanel ballPanel = new BallPanel(this);

    public BallFrame() throws HeadlessException {
        jLabel.setBounds(810, 30, 120, 30);
        coordinates.setBounds(810, 60, 120, 30);
        add(jLabel);
        add(coordinates);
        setLayout(null);
        setSize(910, 700);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        ballPanel.setBounds(0, 0, 800, 650);
        add(ballPanel);
    }

    public void setText(String text) {
        this.jLabel.setText(text);
    }
}
