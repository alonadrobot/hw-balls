package com.Ball;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BallPanel extends JPanel {
    int threadCount = 0;

    public BallPanel(BallFrame ballFrame) {
        setLayout(null);
        setVisible(true);
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                Ball ball = new Ball(e.getPoint(), ballFrame.ballPanel);
                add(ball);
                Thread thread = new Thread(ball);
                thread.start();
                threadCount++;
                ballFrame.setText("Ball count: " + threadCount);
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                ballFrame.coordinates.setText("x - " + e.getPoint().x + ",y - " + e.getPoint().y);
            }
        });
    }
}
