package com.Ball;

import javax.swing.*;
import java.awt.*;
import java.security.SecureRandom;

public class Ball extends JPanel implements Runnable{
    private final SecureRandom random = new SecureRandom();

    private final Point point;

    private final Color color;

    private final int ovalSize = random.nextInt((150 - 20) + 1) + 20;

    private int dx;

    private int dy;

    public Ball(Point point, JPanel panel) {
        this.color = new Color(random.nextInt(255), random.nextInt(255), random.nextInt(255));
        this.dx = this.random.nextInt((20 - 5) + 1) - 5;
        this.dy = this.random.nextInt((20 - 5) + 1) - 5;
        if ((ovalSize + point.x) >= panel.getWidth()) {
            point.x = point.x - ovalSize;
        }
        if (point.x <= 1) {
            point.x = point.x + 2;
        }
        if (point.y <= 1) {
            point.y = point.y + 2;
        }
        if ((ovalSize + point.y) >= panel.getHeight()) {
            point.y = point.y - ovalSize;
        }
        this.point = point;
        setSize(ovalSize, ovalSize);
        setOpaque(false);
    }

    public void move() {
        JPanel jPanel = (JPanel) getParent();
        if (this.point.x <= 1 || this.point.x + ovalSize >= jPanel.getWidth()) {
            dx = -dx;
        }
        if (this.point.y <= 1 || this.point.y + ovalSize >= jPanel.getHeight()) {
            dy = -dy;
        }
        this.point.translate(dx, dy);
        setLocation(point);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.setColor(this.color);
        graphics2D.fillOval(0, 0, ovalSize, ovalSize);
    }

    @Override
    public void run() {
        try {
            while (true) {
                move();
                Thread.sleep(10);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}
